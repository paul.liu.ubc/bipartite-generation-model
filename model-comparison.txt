dataset, actual, synthetic-synthetic, synthetic-binomial
---------------------------------------------------------
actors-movies, 0.3746, 0.3742, 0.2215
amazon-similar-products, 0.6282, 0.5248, 0.5109
authors-papers, 0.3850, 0.4733, 0.4655
classes-drugs, 0.4158, 0.6991, 0.6704
congress-committees, 0.4915, 0.5735, 0.5103
director-boards, 0.5288, 0.2143, 0.3261
diseases-genes, 0.6756, 0.4338, 0.3306
genes-diseases, 0.7562, 0.6395, 0.5792
mathsx-tags-questions, 0.5710, 0.7497, 0.7401
plants-pollinators, 0.8011, 0.8291, 0.7619
posters-forums, 0.6570, 0.6644, 0.5584
women-meetings, 0.8205, 0.7333, 0.7021