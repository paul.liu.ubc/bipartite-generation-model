figure('units','normalized','outerposition',[0 0 1 1])

f = fopen("degree-left.dat");
degree_left = fscanf(f, "%f");
[y, x] = histcounts(log(degree_left));
x = exp(x);
x = x(1:end-1);
x = x(y > 1);
y = y(y > 1);
y = y(x > 0);
x = x(x > 0);
%x = x(floor(end/20):ceil(4*end/10));
%y = y(floor(end/20):ceil(4*end/10));
subplot(2,2,1);
hold off;
scatter(log(x), log(y));
hold on;
title('Degree of left partition');

[b, stats] = robustfit(log(x), log(y));
plot(log(x), b(1) + b(2)*log(x));
tstr = sprintf("y = %f - %f * x", b(1), abs(b(2)));
text(log(x(1)), b(1) + b(2) * log(x(1)), char(tstr));

display(b);
display(mean(degree_left));

f = fopen("degree-right.dat");
degree_right = fscanf(f, "%f");
[y, x] = histcounts(log(degree_right));
x = exp(x);
x = x(1:end-1);
x = x(y > 1);
y = y(y > 1);
y = y(x > 0);
x = x(x > 0);
%x = x(floor(end/10):ceil(7.5*end/10));
%y = y(floor(end/10):ceil(7.5*end/10));
subplot(2,2,2);
hold off;
scatter(log(x), log(y));
hold on;
title('Degree of right partition');

%[log(x); ones(size(x))]' \ log(y')
[b, stats] = robustfit(log(x), log(y));
plot(log(x), b(1) + b(2)*log(x));
tstr = sprintf("y = %f - %f * x", b(1), abs(b(2)));
text(log(x(1)), b(1) + b(2) * log(x(1)), char(tstr));

display(b);
display(mean(degree_right));

f = fopen("degree-projected.dat");
degree_projected = fscanf(f, "%f");
weights = degree_projected(1:2:end);
degrees = degree_projected(2:2:end);
[y, x] = histcounts(degrees);
x = (x(1:end-1) + x(2:end)) / 2;
x = x(y > 1);
y = y(y > 1);
y = y(x > 0);
x = x(x > 0);
%x = x(floor(end/30):ceil(5.0*end/10));
%y = y(floor(end/30):ceil(5.0*end/10));
subplot(2,2,3);
hold off;
scatter(log(x), log(y));
hold on;
title('Degree of projection onto left');

%[log(x); ones(size(x))]' \ log(y')
[b, stats] = robustfit(log(x), log(y));
plot(log(x), b(1) + b(2)*log(x));
tstr = sprintf("y = %f - %f * x", b(1), abs(b(2)));
text(log(x(1)), b(1) + b(2) * log(x(1)), char(tstr));
display(b);

f = fopen("ccoef-projected.dat");
ccoef_projected = fscanf(f, "%f");
weights = ccoef_projected(1:4:end);
ccoef = ccoef_projected(3:4:end);
error = ccoef_projected(4:4:end);
[x, p] = sort(weights);
y = ccoef(p);
err = error(p);
% z = proj(p);
% x = x(z>0);
% y = y(z>0);
x = x(y>0);
y = y(y>0);
err = err(y>0);
%y = y(x > 30);
%x = x(x > 30);
%x = x(end-50:end);
%y = y(end-50:end);
subplot(2,2,4);
hold off;
scatter(log(x), log(y));
hold on;
errorbar(log(x), log(y), (log(y+err)-log(max(0.01, y-err)))/2, 'LineStyle', 'none');

[b, stats] = robustfit(log(x), log(y));
px = [log(min(x))-0.3; log(x); log(max(x))+0.3];
plot(px, b(1) + b(2)*px);
tstr = sprintf("y = %f + %f * x", b(1), b(2));
text(px(1), 0.9 * (b(1) + b(2) * px(1)), char(tstr));
title('Sample weight (~degree) vs clustering coefficient');

display(b);