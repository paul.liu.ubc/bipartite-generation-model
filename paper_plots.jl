using PyPlot
using StatsBase
using LaTeXStrings

struct PathStats
    w::Vector{Float64}
    tris::Vector{Int64}
    wedges::Vector{Int64}
    paths2::Vector{Int64}
    degs::Vector{Int64}
end

function read_data(fname)
    # One line per node of the projected graph.
    w = Float64[]
    tris = Int64[]
    wedges = Int64[]
    paths2 = Int64[]
    degs = Int64[]
    open(fname) do f
        for line in eachline(f)
            node_stats = split(line)
            push!(w,      parse(Float64, node_stats[1]))
            push!(tris,   parse(Int64,   node_stats[2]))
            push!(wedges, parse(Int64,   node_stats[3]))
            push!(paths2, parse(Int64,   node_stats[4]))            
            push!(degs,   parse(Int64,   node_stats[5]))
        end
    end
    return PathStats(w, tris, wedges, paths2, degs)
end

function cc_hist(degs::Vector{Int64}, ccs::Vector{Float64})
    maxd = maximum(degs)
    bins = [2:4; 6; 9; exp.(range(log.(12 + 1e-1), log.(maxd + 1), length=18))]

    function find_bin(x::Int64)
        curr = 1
        for (i, v) in enumerate(bins)
            if x <= v
                return i
            end
        end
        error("could not find bin")
    end

    nbins = length(bins)
    binned_degs = [Int64[] for _ in 1:nbins]
    binned_ccs = [Float64[] for _ in 1:nbins]

    for (d, cc) in zip(degs, ccs)
        b = find_bin(d)
        push!(binned_degs[b], d)
        push!(binned_ccs[b], cc)        
    end

    return ([mean(bin) for bin in binned_degs], [mean(bin) for bin in binned_ccs])
end

# plot_ccfs_dist("so-tags-questions")
function plot_ccfs_dist(dataset::String)
    close()

    function get_stats(version::String)
        stats = read_data("output/$(dataset)-$(version)/path-stats.dat")
        keep = findall(stats.degs .>= 2)
        return PathStats(stats.w[keep], stats.tris[keep], stats.wedges[keep], stats.paths2[keep], stats.degs[keep])
    end
    
    stats0 = get_stats("0")
    stats1 = get_stats("1")
    stats2 = get_stats("pl")

    ds0, ccs0 = cc_hist(stats0.degs, stats0.tris ./ stats0.wedges)
    ds1, ccs1 = cc_hist(stats1.degs, stats1.tris ./ stats1.wedges)
    ds2, ccs2 = cc_hist(stats2.degs, stats2.tris ./ stats2.wedges)    

    plot(ds1, ccs1, lw=3, marker="s", ms=8, color="#1b9e77", label="data", clip_on=false)
    plot(ds0, ccs0, lw=2, marker="o", ms=6, color="#d95f02", label="synth-data", clip_on=false)
    plot(ds2, ccs2, lw=1, marker="x", ms=10, color="#7570b3", label="synth-PL", clip_on=false)    

    fsz=24
    ax = gca()
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.tick_params(which="major", labelsize=fsz-2, length=7, width=4)
    ax.tick_params(which="minor", labelsize=fsz-2, length=4, width=2)    
    ylim = (0.95 * min(minimum(ccs0), minimum(ccs1), 1e-1), 1.1)
    ax.set_ylim(ylim)

    title("$dataset", fontsize=fsz)
    if dataset == "genes-diseases"
        legend(loc="lower left", frameon=false, fontsize=fsz, labelspacing=0.2, handletextpad=0.3)
    end
    xlabel("degree", fontsize=fsz, labelpad=0)
    ylabel("clust. coeff.", fontsize=fsz-2, labelpad=0)
    tight_layout()
    savefig("$dataset-ccfs.eps")
end

function plot_h_dist(dataset::String)
    close()

    function get_stats(version::String)
        stats = read_data("output/$(dataset)-$(version)/path-stats.dat")
        keep = findall(stats.paths2 .>= 1)
        return PathStats(stats.w[keep], stats.tris[keep], stats.wedges[keep], stats.paths2[keep], stats.degs[keep])
    end
    
    stats0 = get_stats("0")
    stats1 = get_stats("1")
    stats2 = get_stats("pl")

    ds0, ccs0 = cc_hist(stats0.degs, stats0.tris ./ stats0.paths2)
    ds1, ccs1 = cc_hist(stats1.degs, stats1.tris ./ stats1.paths2)
    ds2, ccs2 = cc_hist(stats2.degs, stats2.tris ./ stats2.paths2)    

    plot(ds1, ccs1, lw=3, marker="s", ms=8, color="#1b9e77", label="data", clip_on=false)
    plot(ds0, ccs0, lw=2, marker="o", ms=6, color="#d95f02", label="synth-data", clip_on=false)
    #plot(ds2, ccs2, lw=1, marker="x", ms=10, color="#7570b3", label="synth-PL", clip_on=false)    

    fsz=24
    ax = gca()
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.tick_params(which="major", labelsize=fsz-2, length=7, width=4)
    ax.tick_params(which="minor", labelsize=fsz-2, length=4, width=2)    
    ylim = (0.95 * min(minimum(ccs0), minimum(ccs1), 1e-1), 1.0)
    ax.set_ylim(ylim)

    title("$dataset", fontsize=fsz)
    if dataset == "genes-diseases"
        legend(loc="lower right", frameon=false, fontsize=fsz-2, labelspacing=0.2, handletextpad=0.3)
    end
    xlabel("degree", fontsize=fsz, labelpad=0)
    ylabel("closure coeff.", fontsize=fsz, labelpad=0)
    tight_layout()
    savefig("$dataset-hcfs.eps")
end

function plot_gcc()
    x = [2.0, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75, 4.0] 
    actuals = [[0.639214,0.693818,0.739509,0.766442,0.770764,0.755283,0.718642,0.675096,0.622532],
              [0.285019,0.336348,0.383247,0.423634,0.427822,0.410883,0.365869,0.318267,0.260237]]
    predicts = [[0.639895,0.692330,0.736682,0.764662,0.771522,0.756127,0.720690,0.671059,0.616240],
                 [0.284251,0.334624,0.384714,0.420684,0.430097,0.409309,0.365750,0.313156,0.264100]]
    αLs = [4.0, 2.5]
    strs = ["40", "25"]
    fsz = 20

    close()
    for (αL, actual, predict, str) in zip(αLs, actuals, predicts, strs)
        figure()

        plot(x, actual,  lw=3,   ls="-", marker="s", ms=8, color="#e41a1c", label="actual")
        plot(x, predict, lw=1.5, ls="-", marker="o", ms=6, color="#377eb8", label="predicted")

        xlabel(L"\alpha_R", fontsize=fsz)
        ylabel("global clust. coeff.", fontsize=fsz)
        if str == "40"
            title(L"\alpha_L = 4.0", fontsize=fsz)
        else
            title(L"\alpha_L = 2.5", fontsize=fsz)
        end

        ylim((0,1))

        if αL == 2.5
            legend(loc="upper left", frameon=false, fontsize=fsz-2, labelspacing=0.2, handletextpad=0.3)
        end

        ax = gca()
        ax.set_xticks(x)
        ax.tick_params(which="major", labelsize=fsz-5, length=7, width=3)

        tight_layout()
        savefig("gcc-$str.eps")
    end
end

function plot_lcc()
    function read_ccf_data(fname::String)
        wu = Float64[]
        predicted_cc = Float64[]
        mean_cc = Float64[]
        
        # [1+M2^2/(M1*M3)wu] [weight wu] [mean of all LCCs with this wu]
        open(fname) do f
            for line in eachline(f)
                data = split(line)
                push!(wu, parse(Float64, data[2]))
                push!(predicted_cc, 1.0 / parse(Float64, data[1]))
                push!(mean_cc, parse(Float64, data[3]))
            end
        end

        return wu, predicted_cc, mean_cc
    end

    αL = "2.5"
    cutoff = "0.25"
    close()
    for αL in ["2.5", "3.5", "4.5"]
        for αR in ["3.0", "3.5", "4.0", "4.5"]
            figure()
            wu, preds, means = read_ccf_data("local-clustering/data-500000000-$(αL)-$(αR)-$(cutoff)/ccoef-projected.dat")
            loglog(wu, preds, lw=0.5, label="predicted")
            loglog(wu, means, marker="o", ms=2, label="mean")
            legend()
            title("($αL, $αR)")
            xlabel("weight")
            ylabel("clust. coeff.")
            savefig("ccfs-$αL-$αR.png")
        end
    end
end

