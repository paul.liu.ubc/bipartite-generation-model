% Script for plotting projected degree distribution results

f = fopen("./projected-degree/degree-projected-4.0-4.0.dat");
degree_projected = fscanf(f, "%f");
weights = degree_projected(1:2:end);
degrees = degree_projected(2:2:end);
[x,y,e] = lnbin(degrees, 15);
x = x(y > 0);
y = y(y > 0);
hold off;
subplot(1,2,1);
scatter(log(x), log(y));
hold on;
title('(\alpha_L = 4, \alpha_R = 4)');
set(gca,'fontsize',18)

[b, stats] = robustfit(log(x), log(y));
plot(log(x), b(1) + b(2)*log(x));
tstr = sprintf("y = %f - %f * x", b(1), abs(b(2)));
text(log(x(1)), b(1) + b(2) * log(x(1)), char(tstr), 'fontsize', 18);
display(b);

f = fopen("./projected-degree/degree-projected-2.5-3.5-uncut.dat");
degree_projected = fscanf(f, "%f");
weights = degree_projected(1:2:end);
degrees = degree_projected(2:2:end);
[x,y,e] = lnbin(degrees, 15);
x = x(y > 0);
y = y(y > 0);
hold off;
subplot(1,2,2);
scatter(log(x), log(y));
hold on;
title('(\alpha_L = 2.5, \alpha_R = 3.5)');
set(gca,'fontsize',18)

[b, stats] = robustfit(log(x), log(y));
plot(log(x), b(1) + b(2)*log(x));
tstr = sprintf("y = %f - %f * x", b(1), abs(b(2)));
text(log(x(1)), b(1) + b(2) * log(x(1)), char(tstr), 'fontsize', 18);
display(b);

