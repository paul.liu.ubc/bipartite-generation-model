include("../common.jl")

function main()
    L = Int64[]
    R = Int64[]
    open("raw/hyperedges-mathoverflow-answers.txt") do f
        for (i, line) in enumerate(eachline(f))
            for j in [parse(Int64, v) for v in split(line, ',')]
                push!(R, i)
                push!(L, j)
            end
        end
    end

    # adjust indices
    L = re_index(L)
    R .+= maximum(L)
    
    write_output(L, R, "mo-questions-users")
end
