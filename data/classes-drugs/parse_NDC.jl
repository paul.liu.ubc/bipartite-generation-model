include("../common.jl")

function main()
    nverts = Int64[]
    open("raw/NDC-classes-full-nverts.txt") do f
        for line in eachline(f); push!(nverts, parse(Int64, line)); end
    end
    simps = Int64[]    
    open("raw/NDC-classes-full-simplices.txt") do f
        for line in eachline(f); push!(simps, parse(Int64, line)); end
    end
    
    max_left = maximum(simps)
    right_ind = max_left + 1
    ind = 0
    L = Int64[]
    R = Int64[]
    for nvert in nverts
        for l in simps[(ind + 1):(ind + nvert)]
            push!(L, l)
            push!(R, right_ind)
        end
        ind += nvert
        right_ind += 1
    end

    write_output(L, R, "classes-drugs")
end
