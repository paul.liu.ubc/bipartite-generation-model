include("../common.jl")

function main()
    I, J = Int64[], Int64[]
    open("raw/Newman-Cond_mat_95-99-two_mode.txt") do f
        for line in eachline(f)
            data = split(line)
            push!(I, parse(Int64, data[1]))
            push!(J, parse(Int64, data[2]))
        end
    end

    L = re_index(I, 1)
    R = re_index(J, maximum(L) + 1)

    write_output(L, R, "condmat-authors-papers")
end
