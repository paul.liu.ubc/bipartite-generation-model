include("../common.jl")

function main()
    I, J = Int64[], Int64[]
    open("raw/net2m_2002-05-01.txt") do f
        for line in eachline(f)
            data = split(line)
            push!(I, parse(Int64, data[1]))
            push!(J, parse(Int64, data[2]))
        end
    end

    L = re_index(I, 1)
    R = re_index(J, maximum(L) + 1)
    write_output(L, R, "directors-boards")
end
