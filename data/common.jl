function re_index(inds::Vector{K}, start::Int64=1) where K <:Union{String,Int64}
    processed_inds = Dict{K,Int64}()
    last_val = start - 1
    new_inds = Vector{Int64}()
    for ind in inds
        if !haskey(processed_inds, ind)
            processed_inds[ind] = last_val + 1
            last_val += 1
        end
        push!(new_inds, processed_inds[ind])
    end
    return new_inds
end

function write_output(L::Vector{Int64}, R::Vector{Int64},
                      name::String)
    open("output/bip-$name.txt", "w") do f
        for (l, r) in zip(L, R); write(f, "$l $r\n"); end
    end
    open("output/L-$name.txt", "w") do f
        for l in minimum(L):maximum(L); write(f, "$l\n"); end
    end
    open("output/R-$name.txt", "w") do f
        for r in minimum(R):maximum(R); write(f, "$r\n"); end
    end
end
