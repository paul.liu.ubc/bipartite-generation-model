include("../common.jl")
using LightXML

function main()
    I = Int64[]
    J = Int64[]
    open("raw/diseasome-parsed.txt") do f
        for line in eachline(f)
            xml_line = parse_string(line)
            data = attributes_dict(root(xml_line))
            push!(I, parse(Int64, data["source"]))
            push!(J, parse(Int64, data["target"]))
        end
    end

    L = re_index(I, 1)
    R = re_index(J, maximum(L) + 1)
    write_output(L, R, "diseases-genes")
end
