include("../common.jl")

function main()
    I, J = Int64[], Int64[]
    open("raw/out.actor-movie") do f
        for line in eachline(f)
            if length(line) >= 1 && line[1] == '%'; continue; end
            data = split(line)
            push!(I, parse(Int64, data[2]))
            push!(J, parse(Int64, data[1]))
        end
    end

    L = re_index(I, 1)
    R = re_index(J, maximum(L) + 1)
    write_output(L, R, "actors-movies")    
end
