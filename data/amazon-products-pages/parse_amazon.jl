include("../common.jl")

function main()
    Id = nothing
    ASIN = nothing
    similar = nothing

    I = String[]
    J = Int64[]
    function parse_current()
        if Id != nothing && ASIN != nothing
            push!(I, String(ASIN))
            push!(J, Id)
            if similar != nothing
                for s in similar
                    push!(I, String(s)); 
                    push!(J, Id)                    
                end
            end
        end
        Id = nothing
        ASIN = nothing
        similar = nothing
    end
    
    open("raw/amazon-parsed.txt") do f
        for line in eachline(f)
            if length(line) > 3 && line[1:3] == "Id:"
                parse_current()
                Id = parse(Int64, strip(line[4:end]))
            end
            if length(line) > 5 && line[1:5] == "ASIN:"
                ASIN = strip(line[6:end])
            end
            if length(line) > 10 && line[1:10] == "  similar:"
                similar = split(strip(line[11:end]))[2:end]
            end
        end
    end

    L = re_index(I, 1)
    R = re_index(J, maximum(L) + 1)
    write_output(L, R, "amazon-products-pages")
end
