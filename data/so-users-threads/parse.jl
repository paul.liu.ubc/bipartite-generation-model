include("../common.jl")

function main()
    nverts = Int64[]
    open("raw/threads-stack-overflow-full-nverts.txt") do f
        for line in eachline(f); push!(nverts, parse(Int64, line)); end
    end
    simps = Int64[]    
    open("raw/threads-stack-overflow-full-simplices.txt") do f
        for line in eachline(f); push!(simps, parse(Int64, line)); end
    end
    
    max_left = maximum(simps)
    ind = 0
    L = Int64[]
    R = Int64[]
    for (i, nvert) in enumerate(nverts)
        for l in simps[(ind + 1):(ind + nvert)]
            push!(L, l)
            push!(R, i)
        end
        ind += nvert
    end

    L = re_index(L)
    R .+= maximum(L)

    write_output(L, R, "so-users-threads")
end
