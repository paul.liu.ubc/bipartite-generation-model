#!/bin/bash

n=500000000
for l in 2.5 3.5 4.5
	do
	for r in 3.0 3.5 4.0 4.5
		do
		./test -l $n -r $n -a $l -b $r -c 0.25;
		mkdir -p ./local-clustering/data-$n-$l-$r-0.25/
		cp *.dat ./local-clustering/data-$n-$l-$r-0.25/
	done
done