import argparse
import powerlaw

def run_analysis(folder_name):
        suffix = folder_name.split('/')[-2]
        graphdat = folder_name + "output/bip-" + suffix + ".txt"
        ldat = folder_name + "output/L-" + suffix + ".txt"
        rdat = folder_name + "output/R-" + suffix + ".txt"
        
        try:
                L = open(ldat)
                R = open(rdat)
                graph = open(graphdat)
        except:
                return

        ldeg, rdeg = dict(), dict()
        
        for u in L.readlines():
                ldeg[int(u)] = 0
        for u in R.readlines():
                rdeg[int(u)] = 0

        for line in graph.readlines():
                for u in line.split():
                        u = int(u)
                        if u in ldeg:
                                ldeg[u] += 1
                        else:
                                rdeg[u] += 1

        ldegs = list(ldeg.values())
        rdegs = list(rdeg.values())
        lfit = powerlaw.Fit(ldegs, discrete=True)
        rfit = powerlaw.Fit(rdegs, discrete=True)        
        print('Graph: {}, Partition sizes: {} {}'.format(suffix, len(ldeg), len(rdeg)))
        print('Left part: {} +/ {}, min: {}, p: {}'.format(lfit.power_law.alpha, lfit.power_law.sigma, lfit.xmin, lfit.D))
        print('Right part: {} +/ {}, min: {}, p: {}'.format(rfit.power_law.alpha, rfit.power_law.sigma, rfit.xmin, rfit.D))

        
if __name__ == "__main__":
    # Get graph name
        parser = argparse.ArgumentParser(description='Fit a power law to the input distribution of a given bipartite graph.')
        parser.add_argument('folder_name', metavar='name', type=str, 
                            help='The name of the folder containing the graph.')

        args = parser.parse_args()
        run_analysis(args.folder_name)
