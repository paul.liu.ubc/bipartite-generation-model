clf;

f = fopen("./path-stats.dat");
data = fscanf(f, "%f");
w = data(1:4:end);
tri = data(2:4:end);
wedge = data(3:4:end);
path = data(4:4:end);

subplot(1,4,1);
hold on;

z = 0;
for i=1:max(w)
    if sum(w==i) < 5
        break
    end
    z(i) = sum(tri(w==i)) / sum(w==i);
end
scatter(log(w(w<=length(z))), log(tri(w<=length(z))));
plot(log(1:length(z)), log(z), 'g-', 'LineWidth', 4);

subplot(1,4,2);
hold on;

t = 0;
for i=1:max(w)
    if sum(w==i) < 5
        break
    end
    t(i) = sum(wedge(w==i)) / sum(w==i);
end
scatter(log(w(w<=length(t))), log(wedge(w<=length(t))));
plot(log(1:length(t)), log(t), 'g-', 'LineWidth', 4);

subplot(1,4,3);
hold on;

p = 0;
for i=1:max(w)
    if sum(w==i) < 5
        break
    end
    p(i) = sum(path(w==i)) / sum(w==i);
end
scatter(log(w(w<=length(p))), log(path(w<=length(p))));
plot(log(1:length(p)), log(p), 'g-', 'LineWidth', 4);


subplot(1,4,4);
hold on;

m = min(length(p), length(z));
plot(log(1:m), log(2*z(1:m)./p(1:m)), 'g-', 'LineWidth', 4);

