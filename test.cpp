#if !defined(__clang__)
#include <bits/stdc++.h>
#endif
#include <stdlib.h>

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

#include "graph.h"
#include "distribution.h"

using namespace std;
using namespace graph_lib;

int dfs(vector<int>& found, Graph& G, int u, int id) {
	if (found[u]) return 0;
	found[u] = id;
	int res = 0;
	stack<int> S;
	S.push(u);

	while (!S.empty()) {
		u = S.top();
		S.pop();
		for (auto e : G[u]) {
			if (found[e.dst]) continue;
			found[e.dst] = id;
			res++;
			S.push(e.dst);
		}
	}
	return 1 + res;
}

int main(int argc, char* argv[]) {
	extern char *optarg;
	static char usage[] = "usage: %s -f filename -e test_type -l nleft -r nright [-d distribution_type -a left_power -b right_power -x lxmin -y rxmin -c cutoff]\n";

	int flags = 0, c, nl, nr, type = 0, model_type = 0;
	double lp = -3, rp = -4, cutoff = 1.0;
	int lxmin = 1, rxmin = 1;
	string filename;
	while ((c = getopt(argc, argv, "a:b:c:d:e:f:l:r:x:y:")) != -1) {
		switch (c) {
			case 'l':
				flags = 1;
				nl = atoi(optarg);
				break;
			case 'r':
				flags = 2;
				nr = atoi(optarg);
				break;
			case 'f':
				flags = 3;
				filename = string(optarg);
				break;
			case 'e':
				model_type = atoi(optarg);
				break;
			case 'a':
				lp = atof(optarg);
				break;
			case 'b':
				rp = atof(optarg);
				break;
              		case 'x':
                                lxmin = atoi(optarg);
				break;
                	case 'y':
                                rxmin = atoi(optarg);
				break;
			case 'c':
				cutoff = atof(optarg);
				break;
			case 'd':
				type = atoi(optarg);
				break;
			case '?':
				break;
		}
	}

	if (flags < 1) {	/* missing mandatory options */
		fprintf(stderr, "%s: missing options\n", argv[0]);
		fprintf(stderr, usage, argv[0]);
		exit(1);
	}

	shared_ptr<DistributionType> left, right;

	BipartiteGraph BG(0, 0);
	map<int, int> deg_bp;
	unordered_set<int> lnodes, rnodes;
	if (flags < 3) {
	        left = make_shared<ZipfDistribution>(-lp, lxmin, nr);
		cutoff_distribution(left.get(), int(pow(nr, cutoff)));
		if (type == 0) {
		        right = make_shared<ZipfDistribution>(-rp, rxmin, nl);
			cutoff_distribution(right.get(), int(pow(nr, cutoff)));
		} else {
			right = make_shared<BinomialDistribution>(left->get_mean() / nr, nl);
		}
	} else {
		string suffix;
		int cnt = 0;
		for (int i = 0; i < (int) filename.size(); i++) {
			if (filename[filename.size() - i - 1] == '/') {
				cnt++;
				if (cnt == 2) {
					suffix = filename.substr(filename.size() - i);
					break;
				}
			}
		}
		if (suffix.empty()) {
			suffix = filename;
		}
		suffix.pop_back();

		ifstream graphdat(filename + "output/bip-" + suffix + ".txt", ifstream::in);
		ifstream ldat(filename + "output/L-" + suffix + ".txt", ifstream::in);
		ifstream rdat(filename + "output/R-" + suffix + ".txt", ifstream::in);

		int u, v;
		long long nedges = 0;
		while (ldat >> u) lnodes.insert(u);
		while (rdat >> v) rnodes.insert(v);

		BG.update(lnodes.size(), rnodes.size());
		while (graphdat >> u >> v) {
			deg_bp[u]++;
			deg_bp[v]++;
			nedges++;

			if (rnodes.count(u)) swap(u, v);
			BG.add_edge(u-1, v-lnodes.size()-1);
		}

		vector<double> lweights(rnodes.size() + 1);
		vector<double> rweights(lnodes.size() + 1);
		nl = lnodes.size();
		nr = rnodes.size();

		for (int u : lnodes) {
			lweights[deg_bp[u]]++;
		}
		for (int v : rnodes) {
			rweights[deg_bp[v]]++;
		}

		left = make_shared<DistributionType>(lweights);
		right = make_shared<DistributionType>(rweights);
		
		int nnodes = deg_bp.size();
		cout << "read in a graph with " << nnodes << " " << nedges << " edges" << endl;
  		cout << "Average degree: " << nedges / nnodes << endl;
  		cout << "Partition sizes: " << nl << " " << nr << endl;
	}

	if (model_type == 2) {
		right = make_shared<BinomialDistribution>(left->get_mean() / nr, nl);
	}

	cout << "Expected number of edges: " << int(left->get_mean() * nl) << " " << int(right->get_mean() * nr) << endl;
	GraphGenerator* bipartite_gen = new GraphGenerator(left, right);
	auto result = bipartite_gen->generate_graph_fast();
    delete bipartite_gen;

	BipartiteGraph G = get<0>(result);
	vector<double> wl = get<1>(result), wr = get<2>(result);
	cout << "Done generating bipartite graph..." << endl;

	if (model_type == 1) {
		cout << "Replacing generated bipartite graph with actual graph..." << endl;
		G = BG;
	}

	// Save test configuration
	ofstream test_info("test_info.dat", ofstream::out);
	test_info << "Bipartite Graph: " << nl << " " << nr << " " << type << endl;
	test_info << "BG parameters: " << lp << " " << rp << " " << cutoff << endl;
	test_info << "Input graph (none means no graph): " << filename << " " << model_type << endl;

	auto Gp = G.project_fast();
	cout << "Done projecting graph to left partition..." << endl;
	
#ifdef OUTPUT_DEGREES
	// Test Theorem 4.4
	// Check if degree distributions on left and right follow power laws
	ofstream degree_left("degree-left.dat", ofstream::out);
	for (int i = 0; i < G.n; i++) {
		degree_left << G.left[i].size() << endl;
	}
	degree_left.close();

	ofstream degree_right("degree-right.dat", ofstream::out);
	for (int i = 0; i < G.m; i++) {
		degree_right << G.right[i].size() << endl;
	}
	degree_right.close();
	
	// Test Theorem 4.12 and 4.13
	ofstream degree_projected("degree-projected.dat", ofstream::out);
	for (size_t i = 0; i < Gp.size(); i++) {
		degree_projected << wl[i] << " " << Gp[i].size() << endl;
	}
	degree_projected.close();
#endif

#define MEM_OPTIMAL 1
// Clean up memory for low mem machines
#ifdef MEM_OPTIMAL
    vector<vector<int>>().swap(G.left);
    vector<vector<int>>().swap(G.right);
#endif

	// Test Theorem 4.14
	// Isolate largest component
	// vector<int> found(Gp.size());
	// vector<bool> include(Gp.size());
	// int largest = 0, lid = 0, np = 0;
	// for (int i = 0; i < Gp.size(); i++) {
	// 	int size = dfs(found, Gp, i, i+1);
	// 	if (size > largest) {
	// 		largest = size;
	// 		lid = i+1;
	// 		cerr << "Found component of size " << largest << endl;
	// 	}
	// }
	// for (int i = 0; i < Gp.size(); i++) {
	// 	if (found[i] == lid) {
	// 		include[i] = true;
	// 	}
	// }
	// np = largest;

    cout << "Computing clustering coefficients..." <<endl;
	ofstream ccoef_projected("ccoef-projected.dat", ofstream::out);
#if OUTPUT_CLOSURE
	ofstream closure_projected("closure-projected.dat", ofstream::out);
#endif
	ofstream ccoef_projected_deg("ccoef-projected-by-degree.dat", ofstream::out);

	map<double, vector<double>> data, data_deg, data_pz;
	double Cavg = 0;
	double Cglb = 0;
	double Ccnt = 0;
	double Havg = 0;
	// double Hcnt = 0;
	double Tavg = 0;
	double Wavg = 0;
	double Hdef = 0;
	// double TWcov = 0;
	// double Wvar = 0;
	int L = int(Gp.size() / 100)+1;
	cerr << "  0.00% completed";
	cerr << flush;

	vector<bool> seen(Gp.size());

#ifdef OUTPUT_PSTATS
	vector<long long> num_tri(Gp.size()), num_wedges(Gp.size()), num_p2(Gp.size());
#endif

	int nexcluded = 0, ndeg0 = 0, nhastri = 0;
	for (size_t i = 0; i < Gp.size(); i++) {
		//if (!include[i]) continue;
		if ((i+L-1)%L == 0) {
			cerr << "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b";
			if (double(i) / Gp.size() < 0.1) cerr << "  ";
			cerr << fixed << setprecision(2) << double(i) / Gp.size() * 100 << "% completed";
			cerr << flush;
		}
		double Cu = 0, Hu = 0, Wu = 0;
		
		long long d = Gp[i].size();
		if (d < 2) {
			data_pz[wl[i]].push_back(1);
			nexcluded++;
		}
		if (d == 0) {
			Cu = 1;
			Hu = 0;
			ndeg0++;
		} else if (d == 1) {
			Cu = 1;
			Wu += Gp[Gp[i][0].dst].size() - 1;
			Wavg += Wu;
			// TWcov += 0;
			// Wvar += Wu * Wu;
			if (Wu > 0) {
				Hu = 0;
				Hdef += 1;
			} else {
				Hu = 0;
			}
		} else {
			for (auto e : Gp[i]) {
				seen[e.dst] = true;
				Wu += Gp[e.dst].size() - 1;
			}

			double ntri = 0; // actually this is twice the # tri
			set<pair<int, int>> found;
			for (auto e_ : Gp[i]) {
				for (auto e : Gp[e_.dst]) {
					if (seen[e.dst]) {
						ntri++;
					}
				}
			}

			ntri /= 2;
			Cu = ntri / (0.5 * d * (d-1));
			Cglb += ntri;
			if (Wu > 0) {
				Hu = 2 * ntri / Wu;
				Tavg += ntri;
				Wavg += Wu;
				// TWcov += 2 * ntri * Wu;
				// Wvar += Wu * Wu;
				Hdef += 1;
#if OUTPUT_CLOSURE
				closure_projected << wl[i] << " " << Gp[i].size() << " " << Hu << endl;
#endif
			} else {
				Hu = 0;
			}
			
			for (auto e : Gp[i]) {
				seen[e.dst] = false;
			}

#ifdef OUTPUT_PSTATS
			num_tri[i] = ntri;
#endif

			nhastri += (ntri > 0);

			data_pz[wl[i]].push_back(0);
		}
		Cavg += Cu;
		Ccnt += 0.5 * d * (d - 1);
		Havg += Hu;
		// Hcnt += 1;
		// ccoef_projected << Gp[i].size() << " " << scale << " " << " " << wl[i] << " " << Cu << endl;
		data[wl[i]].push_back(Cu);
		data_deg[d].push_back(Cu);

#ifdef OUTPUT_PSTATS
		num_wedges[i] = 0.5 * d * (d-1);
		num_p2[i] = Wu;
#endif
	}
	cerr << "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b";
	cerr << "100.00% completed" << endl;

#ifdef OUTPUT_PSTATS
	ofstream path_stats("path-stats.dat", ofstream::out);
	for (size_t i = 0; i < Gp.size(); i++) {
	  path_stats << wl[i] << " " << num_tri[i] << " " << num_wedges[i] << " " << num_p2[i] << " " << Gp[i].size() << endl;
	}
#endif

	cout << "Number of vertices with deg < 2: " << nexcluded << endl;
	cout << "Number of vertices with deg == 0: " << ndeg0 << endl;
	cout << "Number of vertices with defined closure coefficient: " << (long long)(Hdef) << endl;
	cout << "Total number of triangles: " << (long long)(Cglb) << endl;
	Cglb /= Ccnt;
	cout << "Global clustering coefficient: " << fixed << setprecision(6) << Cglb << endl;
	
	auto M = [&](int i, int p) {
		return pow(get_kth_seq_moment(wr, i), p);
	};

	double C = M(2, 2) / (M(3, 1) * M(1, 1));
	double C0 = (-((pow(nr,-1+cutoff)*M(2,1))/M(1,2))+(pow(nr,-2+cutoff)*M(3,1))/M(1,3)+(-M(2,1)*M(3,1)-M(1,1)*M(4,1))/(nr*M(1,2)*M(3,1))+M(5,1)/(pow(nr,2)*M(1,2)*M(3,1))+(pow(nr,-2+cutoff)*M(5,1))/(M(1,2)*M(3,1))-(pow(nr,-3+cutoff)*M(6,1))/(M(1,3)*M(3,1)));
	double C1 = ((3*pow(nr,-2+cutoff)*M(2,2))/(2*M(1,4))+(pow(nr,-2+2*cutoff)*M(2,2))/(2*M(1,4))-M(2,3)/(2*nr*M(1,3)*M(3,1))-(pow(nr,-1+cutoff)*M(2,3))/(2*M(1,3)*M(3,1))-(pow(nr,-3+cutoff)*M(2,1)*M(3,1))/M(1,5)-(pow(nr,-3+2*cutoff)*M(2,1)*M(3,1))/M(1,5)+(pow(nr,-4+2*cutoff)*M(3,2))/(2*M(1,6))-(3*pow(nr,-3+cutoff)*M(4,1))/(2*M(1,4))-(pow(nr,-3+2*cutoff)*M(4,1))/(2*M(1,4))+(3*pow(nr,-2+cutoff)*M(2,1)*M(4,1))/(2*M(1,3)*M(3,1))+(M(2,2)*M(3,1)+3*M(1,1)*M(2,1)*M(4,1))/(2*pow(nr,2)*M(1,4)*M(3,1))+(3*pow(nr,-4+cutoff)*M(5,1))/(2*M(1,5))+(3*pow(nr,-4+2*cutoff)*M(5,1))/(2*M(1,5))-(2*pow(nr,-3+cutoff)*M(2,1)*M(5,1))/(M(1,4)*M(3,1))-(pow(nr,-3+2*cutoff)*M(2,1)*M(5,1))/(M(1,4)*M(3,1))-(pow(nr,-5+2*cutoff)*M(6,1))/M(1,6)-(pow(nr,-3+cutoff)*M(2,2)*M(6,1))/(2*M(1,4)*M(3,2))-(pow(nr,-3+cutoff)*M(6,1))/(M(1,3)*M(3,1))+(pow(nr,-4+cutoff)*M(2,1)*M(6,1))/(M(1,5)*M(3,1))+(pow(nr,-4+2*cutoff)*M(2,1)*M(6,1))/(M(1,5)*M(3,1))+(pow(nr,-4+cutoff)*M(4,1)*M(6,1))/(2*M(1,4)*M(3,2))-(pow(nr,-5+cutoff)*M(5,1)*M(6,1))/(2*M(1,5)*M(3,2))-(pow(nr,-5+2*cutoff)*M(5,1)*M(6,1))/(2*M(1,5)*M(3,2))+(pow(nr,-6+2*cutoff)*M(6,2))/(2*M(1,6)*M(3,2))+(-M(3,1)*M(4,1)-2*M(2,1)*M(5,1)-2*M(1,1)*M(6,1))/(2*pow(nr,3)*M(1,4)*M(3,1))+(2*pow(nr,-4+cutoff)*M(7,1))/(M(1,4)*M(3,1))+(pow(nr,-4+2*cutoff)*M(7,1))/(M(1,4)*M(3,1))-(pow(nr,-5+cutoff)*M(8,1))/(M(1,5)*M(3,1))-(pow(nr,-5+2*cutoff)*M(8,1))/(M(1,5)*M(3,1)));
	
	double PCglb = 1/(1 + C * get_kth_seq_moment(wl, 2) / get_kth_seq_moment(wl, 1));
	cout << "Predicted global clustering coefficient: " << fixed << setprecision(6) 
		 << PCglb << endl;

	Havg /= Hdef;
	cout << "Average local closure coefficient: " << Havg << endl;
	cout << "Predicted average local closure coefficient: " << fixed << setprecision(6) 
		 << PCglb << endl; 
	
	//cout << C0 << " " << C1 << " " << C << endl;
	for (auto& kv : data) {
		double wu = kv.first;
		double scale = 1 + wu * C;
		double scale2 = 1 + wu * (C + C0) + wu * wu * C1;
		double p0 = sample_mean(data_pz[wu]);
		ccoef_projected << 1./scale * (1 - p0) + p0 << " " << 1./scale2 * (1 - p0) + p0 << " " << kv.first << " " << sample_mean(kv.second) << " " << sample_std(kv.second) << endl;
	}
	for (auto& kv : data_deg) {
		ccoef_projected_deg << kv.first << " " << sample_mean(kv.second) << " " << sample_std(kv.second) << endl;
	}
	ccoef_projected.close();
#if OUTPUT_CLOSURE
	closure_projected.close();
#endif

	Cavg /= Gp.size();
	test_info << "Average clustering coefficent: " << Cavg << endl;
	test_info.close();

	cout << "Average clustering coefficent: " << fixed << setprecision(4) << Cavg << endl;

	return 0;
}
