#ifndef DISTRIBUTION_H
#define DISTRIBUTION_H

#include "graph.h"

#if defined(__clang__)
#include <random>
#else
#include <bits/stdc++.h>
#endif

using namespace std;
using namespace graph_lib;

double kahan_sum(const vector<double>& vec) {
  double sum = 0.0;
  double c = 0.0;
  for (auto v : vec) {
    double y = v - c;
    double t = sum + y;
    c = (t - sum) - y;
    sum = t;
  }
  return sum;
}

vector<double> kahan_cumsum(const vector<double>& vec) {
  double sum = 0.0;
  double c = 0.0;
  vector<double> ret(vec.size());
  for (uint64_t i = 0; i < vec.size(); i++) {
    double y = vec[i] - c;
    double t = sum + y;
    c = (t - sum) - y;
    sum = t;
    ret[i] = sum;
  }
  return ret;
}

double get_kth_seq_moment(const vector<double>& s, int k) {
  vector<double> scaled(s.size());
  for (uint64_t i = 0; i < s.size(); i++) {
    scaled[i] = pow(s[i], k) / s.size();
  }
  return kahan_sum(scaled);
}


class DistributionType {
public:
	int n;
	vector<double> weights;
	DistributionType(vector<double> weights_) {
		n = weights_.size() - 1;
		weights = weights_;
		weights[0] = 0.0;
		this->reweigh();
	}

	DistributionType(int n_) {
		n = n_;
		weights.resize(n_ + 1);
	}

	int support_size() {
		return n;
	}

	void generate_weights() {
		throw string("this is not implemented");
	}
	
	void reweigh() {
	  double sum = kahan_sum(weights);
	  for (int i = 0; i <= n; i++) {
	    weights[i] /= sum;
	  }
	}

	vector<double> get_value(int nvalues = 1) {
		static default_random_engine generator;
  		static uniform_real_distribution<double> distribution(0.0, 1.0);
  		vector<double> values(nvalues);
  		for (int i = 0; i < nvalues; i++) {
  			values[i] = distribution(generator);
  		}
  		sort(values.begin(), values.end());
		vector<double> cumsum_weights = kahan_cumsum(weights);
  		int j = 0;
  		for (int i = 0; i <= n; i++) {
		  double s = cumsum_weights[i];
		  while (j < nvalues && values[j] < s) {
		    values[j] = i;
		    j++;
		  }
		  if (j == nvalues) break;
  		}
  		return values;
	}

	double get_mean() {
	  vector<double> scaled(weights.size());
	  for (uint64_t i = 0; i < weights.size(); i++) {
	    scaled[i] = i * weights[i];
	  }
	  return kahan_sum(scaled);
	}
};

class BinomialDistribution : public DistributionType {
public:
	double p;
	BinomialDistribution(double p_, int n_) : DistributionType(n_) {
		p = p_;
		this->generate_weights();
		this->reweigh();
	}

	void generate_weights() {
		// logpi = log( p * p_{i-1} / (1-p) * (n-k+1) / k )
		double logpi = n * log(1 - p);
		for (int i = 0; i <= n; i++) {
			weights[i] = exp(logpi);
			logpi = log(p) + logpi - log(1-p);
			if (i != n) logpi += log(n-i);
			if (i != 0) logpi -= log(i+1);
		}
	}
};

class ZipfDistribution : public DistributionType {
public:
	double alpha;
        int xmin;

        ZipfDistribution(double alpha_, int n_) : DistributionType(n_), xmin(1) {
		alpha = alpha_;
		this->generate_weights();
	}
  
        ZipfDistribution(double alpha_, int xmin_, int n_) : DistributionType(n_) {
	        alpha = alpha_;
		xmin = xmin_;
		this->generate_weights();
	}

	void generate_weights() {
                for (int i = 0; i < xmin; i++) {
		        weights[i] = 0;
                }
		for (int i = xmin; i <= n; i++) {
			weights[i] = pow(i, alpha);
		}
		this->reweigh();
	}
};

void cutoff_distribution(DistributionType* dtype, int cutoff) {
	for (int i = cutoff; i <= dtype->n; i++) {
		dtype->weights[i] = 0;
	}
	dtype->reweigh();
}

double sample_mean(vector<double>& S) {
  return get_kth_seq_moment(S, 1);
}

double sample_std(vector<double>& S) {
	double mean = sample_mean(S);
	vector<double> normalized(S.size());
	for (uint64_t i = 0; i < S.size(); i++) {
	  normalized[i] = S[i] - mean;
	}
	return sqrt(get_kth_seq_moment(normalized, 2));
}

class GraphGenerator {
public:
	shared_ptr<DistributionType> left, right;
	GraphGenerator(shared_ptr<DistributionType> left, shared_ptr<DistributionType> right) : left(left), right(right) {}

	tuple<BipartiteGraph, vector<double>, vector<double>> generate_graph() {
		// n is the number of nodes on the left, m is the number of nodes on the right
		// the right hand distribution has range [1, #nodes on left]
		size_t n = right->support_size(), m = left->support_size();
		BipartiteGraph G(n, m);
		vector<double> wl = left->get_value(n), wr = right->get_value(m);

		static default_random_engine generator(time(nullptr));
  		static uniform_real_distribution<double> distribution(0.0, 1.0);
  		double scale = m * get_kth_seq_moment(wr, 1);
		for (size_t i = 0; i < n; i++) {
			for (size_t j = 0; j < m; j++) {
				double p = min(wl[i] * wr[j] / double(scale), 1.0);
				double s = distribution(generator);
				if (s <= p) {
					G.add_edge(i, j);
				}
			}
		}
  		cerr << "Edge added: " << G.nedges << endl;

		return make_tuple(G, wl, wr);
	}

	// Efficient graph generation through grouping edges that 
	// appear with equal probability
	tuple<BipartiteGraph, vector<double>, vector<double>> generate_graph_fast() {
		size_t n = right->support_size(), m = left->support_size();
		BipartiteGraph G(n, m);
		vector<double> wl = left->get_value(n), wr = right->get_value(m);

		static default_random_engine generator(time(nullptr));
  		map<double, vector<int>> gl, gr;
  		for (size_t i = 0; i < n; i++) {
  			gl[wl[i]].push_back(i);
  		}

  		for (size_t j = 0; j < m; j++) {
  			gr[wr[j]].push_back(j);
  		}

  		double scale = m * get_kth_seq_moment(wr, 1);
  		for (const auto& xl : gl) {
  			for (const auto& xr : gr) {
  				// Now among groups of equal probability, its just a binomial distribution
  				double p = min((1.0 * xl.first) * xr.first / scale, 1.0);
  				long long totale = xl.second.size() * xr.second.size();
  				binomial_distribution<long long> distribution(totale, p);
  				int ne = distribution(generator);
  				unordered_set<long long> added;
  				for (int i = 0; i < ne; i++) {
  					unsigned long long e = rand64() % totale;
  					while (added.count(e)) {
  						e = rand64() % totale;
  					}
  					int u = e / xr.second.size(), v = e % xr.second.size();
  					G.add_edge(xl.second[u], xr.second[v]);
  				}
  			}
  		}
  		cerr << "Edges added: " << G.nedges << endl;

		return make_tuple(G, wl, wr);
	}
};

#endif
