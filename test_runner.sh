# Example: bash test_runner.sh diseases-genes

dataset=$1

echo $dataset

for i in {0..2}; do
    ./test -f data/$dataset/ -e $i && mkdir -p output/$dataset-$i && mv *.dat output/$dataset-$i
done
