This repository contains experiments for testing a Chung-Lu-type graph generation model for bipartite graphs.

# Building and running the code

To build, just type `make`.

To run, type `./test` to see some program options.

Here are the possible options:
- `-f filename`: Reads in a bipartite graph file from the data folder. **Disregards all the other options except `-e test_type`.**
- `-e test_type`: test_type 0 is synthetic-synthetic with file input, test_type 1 is using the actual graph, test_type 2 is synthetic-binomial with file input
- `-l nleft`: number of vertices of left partition
- `-r nright`: number of vertices of right partition
- `-d distribution_type`: type 0 is synthetic-synthetic, type 1 is synthetic-binomial
- `-a left_power`: degree of power law of left hand side (input of 3 means d^(-3)).
- `-b right_power`: degree of power law of right hand side
- `-c cuttoff`: the degree cutoff parameter (input of 0.3 means cutoff at n^0.3)

Sample commands:
- `./test -f ./data/actor-movies/ -e 0;`
- `./test -l 10000 -r 10000 -a 2.5 -b 3.5 -c 0.3`

# Interpreting the .dat output files

## Description of the .dat files

The `test` program outputs a couple of files thats useful for plotting:
- `test_info.dat`: File containing some experiment parameters.
- `path-stats.dat`: File containing some statistics like sampled weights, number of triangles, number of wedges, etc.
- `degree-left.dat`: File containing the degree distribution of the left partition (two columns: weight and degree).
- `degree-right.dat`: File containing the degree distribution of the right partition (two columns: weight and degree).
- `degree-projected.dat`: File containing the degree distribution of the projected graph (two columns: weight and degree).
- `closure-projected.dat`: File containing closure coefficients (four columns: denom of predicted lcc, weight, lcc mean, lcc variance).
- `ccoeff-projected.dat`: File containing clustering coefficients (three columns: denom. of predicted expression from paper, weight, mean of CC with this weight, stdev of CC with this weight)
- `closure-projected-by-degree.dat`: File containing closure coefficients (three columns: degree, lcc mean, lcc var).

## Format of the .dat files
- `path-stats.dat`: Each line has the format `[weight wu] [#triangles on u] [#wedges on u] [#length 2 paths with u as head] [degree u]` for the projected graph. One line per node of the projected graph.
- `degree-left.dat`: A file of all the vertex degrees of the left partition of the bipartite graph (one line per node).
- `degree-right.dat`: A file of all the vertex degrees of the right partition of the bipartite graph (one line per node).
- `degree-projected.dat`: A file of all the vertex degrees of the projected graph. Each line has the format `[weight wu] [degree u in proj.]`. One line per node.
- `closure-projected.dat`: Each line has the format `[weight wu] [degree of u] [local closure coef]`. One line per node in the projected graph.
- `ccoeff-projected.dat`: Each line has the format `[1+M2^2/(M1*M3)wu] [weight wu] [mean of all LCCs with this wu] [std of all LCCs with this wu]`. One line per unique `wu` sampled.
- `ccoeff-projected-by-degree.dat`: Each line has the format `[degree of node] [mean of all LCCs with this degree] [std of all LCCs with this degree]`. One line per unique `degree` sampled.

