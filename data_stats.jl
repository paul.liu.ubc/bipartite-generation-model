function main()
    datadir = "data"
    for dataset in readdir(datadir)
        if isdir("$datadir/$dataset")
            num_L = countlines("data/$dataset/output/L-$dataset.txt")
            num_R = countlines("data/$dataset/output/R-$dataset.txt")
            num_edges = countlines("data/$dataset/output/bip-$dataset.txt")
            println(join([dataset, num_L, num_R, num_edges], " & "), " \\\\")
        end
    end
end
