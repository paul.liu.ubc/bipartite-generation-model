clf;
x = [2.0, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75, 4.0]

subplot(1,2,1);
hold on;

actual = [0.639214,0.693818,0.739509,0.766442,0.770764,0.755283,0.718642,0.675096,0.622532];
predicted = [0.639895,0.692330,0.736682,0.764662,0.771522,0.756127,0.720690,0.671059,0.616240];
plot(x, actual, 'b-', 'LineWidth', 2);
plot(x, predicted, 'r-', 'LineWidth', 2);
legend('actual', 'predicted');

title('\alpha_L = 4');
set(gca,'fontsize',18);


subplot(1,2,2);
hold on;

actual = [0.285019,0.336348,0.383247,0.423634,0.427822,0.410883,0.365869,0.318267,0.260237];
predicted = [0.284251,0.334624,0.384714,0.420684,0.430097,0.409309,0.365750,0.313156,0.264100];
plot(x, actual, 'b-', 'LineWidth', 4);
plot(x, predicted, 'r-', 'LineWidth', 4);
legend('actual', 'predicted');

title('\alpha_L = 2.5');
set(gca,'fontsize',18);
