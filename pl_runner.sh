# Using information from output/pls.txt, which
# was generated by run-graph-analysis.sh

# amazon-similar-products
function run_amazon_similar_products() {
    ./test -l 721338 \
	   -r 548551 \
	   -a 1.793100967333166 \
	   -b 1.3688143653396734 \
	   -x 1 \
	   -y 1 \
	   -c 0.5
    mkdir -p output/amazon-similar-products-pl
    mv *.dat output/amazon-similar-products-pl
}

function run_amazon_similar_products2() {
    ./test -l 721338 \
	   -r 548551 \
	   -a 3.4266372750650786 \
	   -b 1.5305550700099428 \
	   -x 23 \
	   -y 1 \
	   -c 0.5
    mkdir -p output/amazon-similar-products-pl-xmin
    mv *.dat output/amazon-similar-products-pl-xmin
}

# actors-movies
function run_actors_movies() {
    ./test -l 383640 \
	   -r 127823 \
	   -a 1.793100967333166 \
	   -b 1.3688143653396734 \
	   -x 1 \
	   -y 1 \
	   -c 0.5
    mkdir -p output/actors-movies-pl
    mv *.dat output/actors-movies-pl
}

# actors-movies
function run_actors_movies2() {
    ./test -l 383640 \
	   -r 127823 \
	   -a 1.8623650345000595 \
	   -b 5.066327861107771 \
	   -x 2 \
	   -y 48 \
	   -c 0.5
    mkdir -p output/actors-movies-pl-xmin
    mv *.dat output/actors-movies-pl-xmin
}

# genes-diseases
function run_genes_diseases() {
    ./test -l 1419 \
	   -r 516 \
	   -a 1.7325675874033708 \
	   -b 1.7595610173333873 \
	   -x 1 \
	   -y 2 \
	   -c 0.5
    mkdir -p output/genes-diseases-pl
    mv *.dat output/genes-diseases-pl
}

# genes-diseases
function run_genes_diseases2() {
    ./test -l 1419 \
	   -r 516 \
	   -a 3.105542728529128 \
	   -b 3.1205884435355635 \
	   -x 10 \
	   -y 5 \
	   -c 0.5
    mkdir -p output/genes-diseases-pl-xmin
    mv *.dat output/genes-diseases-pl-xmin
}

#run_amazon_similar_products
#run_actors_movies
run_genes_diseases
#run_amazon_similar_products2
#run_actors_movies2
run_genes_diseases2
