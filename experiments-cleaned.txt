statistics
----------
dataset, left-partition-size, right-partition-size, num-edges
actors-movies,127823,383640,1470404
amazon-similar-products,721338,548551,2337271
authors-papers,16726,22015,58595
classes-drugs,1161,49726,156251
congress-committees,871,897,17814
directors-boards,204,1013,1130
diseases-genes,516,1419,3926
genes-diseases,1419,516,3926
mathsx-tags-questions,1629,822059,1801637
plants-pollinators,56,9,103
posters-forums,899,522,7089
women-meetings,18,14,89

local-clustering
----------------
dataset, real, synthetic-synthetic, synthetic-binomial
actors-movies,0.4159,0.3991,0.2578
amazon-similar-products,0.8053,0.6378,0.6405
authors-papers,0.7787,0.6361,0.6236
classes-drugs,0.8723,0.8052,0.796
congress-committees,0.4993,0.5966,0.5135
directors-boards,0.8386,0.3684,0.3373
diseases-genes,0.8192,0.5043,0.38
genes-diseases,0.8633,0.7208,0.6553
mathsx-tags-questions,0.6321,0.7996,0.7998
plants-pollinators,0.9058,0.909,0.8252
posters-forums,0.7002,0.7088,0.5935
women-meetings,0.9367,0.8857,0.8133

global-clustering
-----------------
dataset, real, synthetic-synthetic, synthetic-binomial
actors-movies,0.217178,0.138512,0.047622
amazon-similar-products,0.202032,0.080772,0.085738
authors-papers,0.35959,0.120169,0.103869
classes-drugs,0.49663,0.498399,0.488721
congress-committees,0.423904,0.555704,0.474883
directors-boards,0.390698,0.196481,0.200682
diseases-genes,0.629625,0.306128,0.19126
genes-diseases,0.659217,0.385454,0.228165
mathsx-tags-questions,0.333255,0.457925,0.465628
plants-pollinators,0.838846,0.831136,0.633996
posters-forums,0.504932,0.496796,0.328469
women-meetings,0.928396,0.851927,0.746681

local-closure
-------------
dataset, real, synthetic-synthetic, synthetic-binomial
actors-movies,0.157726,0.094279,0.044978
amazon-similar-products,0.288366,0.087888,0.089678
authors-papers,0.353337,0.101068,0.096341
classes-drugs,0.399548,0.239839,0.230837
congress-committees,0.386039,0.478218,0.401592
directors-boards,0.266645,0.157965,0.174399
diseases-genes,0.521293,0.208211,0.143925
genes-diseases,0.54086,0.234113,0.190828
mathsx-tags-questions,0.170867,0.253646,0.262707
plants-pollinators,0.683262,0.71707,0.574174
posters-forums,0.338555,0.347737,0.218683
women-meetings,0.917627,0.805429,0.696906
