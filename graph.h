#ifndef GRAPH_H
#define GRAPH_H

#if defined(__clang__)
#include <fstream>
#include <iostream>
#include <unordered_set>
#include <utility>
#include <vector>
#include <queue>
#include <iostream>
#include <map>
#include <set>
#include <random>
#else
#include <bits/stdc++.h>
#endif


using namespace std;

namespace graph_lib {

unsigned long long rand64() {
  static mt19937_64 gen (random_device{}());
  return gen();
}

// generic data structures for graphs
struct full_edge {
  int src, dst, wt;
  bool operator<(const full_edge& o) const {
    if (o.wt != wt) return wt < o.wt;
    return std::make_pair(src, dst) < std::make_pair(o.src, o.dst);
  }
};

struct half_edge {
  int dst, wt;
  const bool operator<(const half_edge& o) const {
    if (dst == o.dst) return wt > o.wt;
    return dst < o.dst;
  }
};

typedef vector<vector<half_edge>> Graph;
typedef map<int, vector<half_edge>> MappedGraph;

struct BipartiteGraph {
  int n, m;
  size_t nedges;
  vector<vector<int>> left, right;
  BipartiteGraph(int n_, int m_) {
    n = n_;
    m = m_;
    left.resize(n);
    right.resize(m);
    nedges = 0;
  }
  void add_edge(int u, int v) {
    left[u].push_back(v);
    right[v].push_back(u);
    nedges++;
  }
  void update(int n_, int m_) {
    n = n_, m = m_, nedges = 0;
    left.clear();
    right.clear();
    left.resize(n);
    right.resize(m);
  }

  // Projects the bipartite graph left
  Graph project() {
    Graph G(n);
    for (int i = 0; i < n; i++) {
      for (int j = i+1; j < n; j++) {
        set<int> s(left[i].begin(), left[i].end());
        bool nbr = false;
        for (int u : left[j]) {
          if (s.count(u)) {
            nbr = true;
            break;
          }
        }

        if (nbr) {
          G[i].push_back({j, 0});
          G[j].push_back({i, 0});
        }
      }
    }

    return G;
  }

  // Projects the bipartite graph left
  Graph project_fast() {
    Graph G(n);
    vector<unordered_set<int>> seen(n);
    for (int j = 0; j < m; j++) {
      for (int u : right[j]) {
        for (int v : right[j]) {
          if (u != v && !seen[u].count(v)) {
            G[u].push_back({v, 0});
            seen[u].insert(v);
          }
        }
      }
    }

    return G;
  }
};

struct degeneracy_info {
  map<int, int> degenFreq;
  vector<int> degenOrder;
};

// computes the degeneracy ordering of a graph G
degeneracy_info compute_degeneracy(Graph& G) {
  const int n = G.size();

  priority_queue<pair<int, int>> pq;
  vector<bool> done(n);
  vector<int> degree(n);
  for (int i = 0; i < n; i++) {
    degree[i] = G[i].size();
    pq.push(make_pair(-degree[i], i));
  }

  map<int, int> dfreq;
  int degeneracy = 0, cnt = 0;
  vector<int> degenOrder;
  while (!pq.empty()) {
    auto p = pq.top();
    pq.pop();
    int u = p.second;
    if (done[u]) continue;

    degenOrder.push_back(u);
    cnt++;
    degeneracy = max(degeneracy, -p.first);
    dfreq[-p.first]++;
    done[u] = true;
    for (auto v : G[u]) {
      if (done[v.dst]) continue;
      degree[v.dst]--;
      pq.push(make_pair(-degree[v.dst], v.dst));
    }
  }

  degeneracy_info retval;
  retval.degenFreq = dfreq;
  retval.degenOrder = degenOrder;
  return retval;
}

// filename contains the path and common prefix of the datafiles
Graph read_graph(string filename) {
  ifstream simplices(filename + "-simplices.txt", ifstream::in);
  ifstream nverts(filename + "-nverts.txt", ifstream::in);
  ifstream times(filename + "-times.txt", ifstream::in);

  cerr << "reading in graph " << filename << endl;

  // no use for the times right now
  map<int, map<int, int>> weight;
  map<int, int> label;
  int ns, nnodes = 0;
  while (nverts >> ns) {
    vector<int> simplex(ns);
    for (int i = 0; i < ns; i++) {
      simplices >> simplex[i];
      if (!label.count(simplex[i])) {
        label[simplex[i]] = nnodes++;
      }
    }

    for (int i = 0; i < ns; i++) {
      for (int j = i+1; j < ns; j++) {
        weight[simplex[i]][simplex[j]]++;
        weight[simplex[j]][simplex[i]]++;
      }
    }
  }

  int nedges = 0;
  Graph G(nnodes);
  for (auto& e0 : weight) {
    for (auto& e1 : e0.second) {
      int u = label[e0.first], v = label[e1.first], w = e1.second;
      G[u].push_back({v, w});
      nedges++;
    }
  }

  cerr << "read in a graph with " << nnodes << " " << nedges / 2 << " edges" << endl;
  cerr << "Average degree: " << nedges / nnodes << endl;
  return G;
}

vector<full_edge> extract_edges(Graph& G) {
	vector<full_edge> res;
	for (int i = 0; i < (int) G.size(); i++) {
		for (auto e : G[i]) {
			if (e.dst > i) {
				res.push_back({i, e.dst, e.wt});
			}
		}
	}

	return res;
}

}

#endif /* GRAPH_H */
