% Script for plotting projected degree distribution results

f = fopen("./local-clustering/ccoef-projected-4.0-4.0-2.dat");
ccoef_projected = fscanf(f, "%f");

denom = ccoef_projected(1:4:end);
weights = ccoef_projected(2:4:end);
ccoef = ccoef_projected(3:4:end);
error = ccoef_projected(4:4:end);
[x, p] = sort(denom);
y = ccoef(p);
err = error(p);
x = x(y>0);
y = y(y>0);
err = err(y>0);
subplot(1,2,1);
hold off;
scatter(log(x), log(y));
hold on;
errorbar(log(x), log(y), (log(y+err)-log(max(0.01, y-err)))/2, 'LineStyle', 'none');

[b, stats] = robustfit(log(x), log(y));
px = [log(min(x))-0.3; log(x); log(max(x))+0.3];
plot(px, b(1) + b(2)*px);
tstr = sprintf("y = %f + %f * x", b(1), b(2));
text(px(1), 0.9 * (b(1) + b(2) * px(1)), char(tstr), 'fontsize', 18);
title('(\alpha_L = 4, \alpha_R = 4)');
set(gca,'fontsize',18)

f = fopen("./ccoef-projected.dat");
ccoef_projected = fscanf(f, "%f");

denom = ccoef_projected(1:4:end);
weights = ccoef_projected(2:4:end);
ccoef = ccoef_projected(3:4:end);
error = ccoef_projected(4:4:end);
[x, p] = sort(denom);
y = ccoef(p);
err = error(p);
x = x(y>0);
y = y(y>0);
err = err(y>0);
subplot(1,2,2);
hold off;
scatter(log(x), log(y));
hold on;
errorbar(log(x), log(y), (log(y+err)-log(max(0.01, y-err)))/2, 'LineStyle', 'none');

[b, stats] = robustfit(log(x), log(y));
px = [log(min(x))-0.3; log(x); log(max(x))+0.3];
plot(px, b(1) + b(2)*px);
tstr = sprintf("y = %f + %f * x", b(1), b(2));
text(px(1), 0.9 * (b(1) + b(2) * px(1)), char(tstr), 'fontsize', 18);
title('(\alpha_L = 2.5, \alpha_R = 3.5)');
set(gca,'fontsize',18)

