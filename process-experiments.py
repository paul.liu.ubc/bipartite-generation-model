data = open("experiments.txt")

lines = [line.strip() for line in data]

def process_chunk(chunk):
	result = dict()
	result['edges'] = edges = int(chunk[0].split()[-2])
	result['partition'] = partition = tuple(int(x) for x in chunk[2].split()[-2:])
	result['gcc'] = gcc = float(chunk[12].split()[-1])
	result['lclo'] = lclo = float(chunk[14].split()[-1])
	result['lclus'] = lclus = float(chunk[16].split()[-1])
	return result	

i = 0
dataset = dict()
while i < len(lines):
	name = lines[i]
	i += 1
	
	dataset[name] = dict()
	dataset[name]['ours'] = process_chunk(lines[i:i+17])
	i += 17
	
	dataset[name]['real'] = process_chunk(lines[i:i+6] + lines[i+7:i+18])
	i += 18
	
	dataset[name]['config'] = process_chunk(lines[i:i+17])
	i += 17
	
sorted_names = sorted([name for name in dataset])

print('statistics')
print('----------')
print('dataset, left-partition-size, right-partition-size, num-edges')
for name in sorted_names:
	print(name, dataset[name]['real']['partition'][0], dataset[name]['real']['partition'][1], dataset[name]['real']['edges'], sep=',')
print()

print('local-clustering')
print('----------------')
print('dataset, real, synthetic-synthetic, synthetic-binomial')
for name in sorted_names:
	print(name, dataset[name]['real']['lclus'], dataset[name]['ours']['lclus'], dataset[name]['config']['lclus'], sep=',')
print()

print('global-clustering')
print('-----------------')
print('dataset, real, synthetic-synthetic, synthetic-binomial')
for name in sorted_names:
	print(name, dataset[name]['real']['gcc'], dataset[name]['ours']['gcc'], dataset[name]['config']['gcc'], sep=',')
print()

print('local-closure')
print('-------------')
print('dataset, real, synthetic-synthetic, synthetic-binomial')
for name in sorted_names:
	print(name, dataset[name]['real']['lclo'], dataset[name]['ours']['lclo'], dataset[name]['config']['lclo'], sep=',')