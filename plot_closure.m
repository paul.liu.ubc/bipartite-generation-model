clf;

f = fopen("./local-closure/closure-projected-4.0-4.0.dat");
data = fscanf(f, "%f");
w = data(1:3:end);
deg = data(2:3:end);
H = data(3:3:end);

subplot(1,2,1);
hold on;

z = 0;
for i=1:max(w)
    if sum(w==i) < 5
        break
    end
    z(i) = sum(H(w==i)) / sum(w==i);
end
scatter(w(w<=length(z)), H(w<=length(z)));
plot(z, 'g-', 'LineWidth', 4);

title('(\alpha_L = 4, \alpha_R = 4)');
set(gca,'fontsize',18)

f = fopen("./local-closure/closure-projected-2.5-3.5.dat");
data = fscanf(f, "%f");
w = data(1:3:end);
deg = data(2:3:end);
H = data(3:3:end);

subplot(1,2,2);
hold on;

z = 0;
for i=1:max(w)
    if sum(w==i) < 5
        break
    end
    z(i) = sum(H(w==i)) / sum(w==i);
end
scatter(w(w<=length(z)), H(w<=length(z)));
plot(z, 'g-', 'LineWidth', 4);

title('(\alpha_L = 2.5, \alpha_R = 3.5)');
set(gca,'fontsize',18)
