#!/bin/bash

for f in `ls ./data/`
do
	echo $f;
	python3 -W ignore graph-analysis.py ./data/$f/
	echo "--------------------------------------------------------------------";
done
