test: graph.h distribution.h test.cpp
	g++ -std=c++11 -O3 -Wall -o test test.cpp

clean:
	rm test
